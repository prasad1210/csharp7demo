﻿using System;

namespace CSharpNewFeatures.CSharp7
{
    public class Tuples
    {
        // Old way of using Tuples
        public Tuple<string, string> GetPatientNameFromOldTuple(int i)
        {
            return Tuple.Create("John", "Doe");
        }

        /// Requires installation of nuget package "System.ValueTuple"
        /// Example #001
        /// Un-named tuples
        public (string, string, string) GetPatientFromUnNamedTuples(long id) // tuple return type
        {
            return ("Prasad", "U", "Kulkarni"); // tuple literal
        }

        /// Example #002
        /// Named tuples
        public (string first, string middle, string last) GetPatientFromNamedTuples(long id) // tuple return type
        {
            return ("Prasad", "U", "Kulkarni"); // tuple literal
        }
    }
}
