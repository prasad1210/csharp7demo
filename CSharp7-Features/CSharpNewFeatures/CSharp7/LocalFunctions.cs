﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class LocalFunctions
    {
        public int Fibonacci(int x)
        {
            if (x < 0)
            {
                throw new ArgumentException("Less negativity please!", nameof(x));
            }

            return Fib(x).current;

            (int current, int previous) Fib(int i)
            {
                if (i == 0) return (1, 0);
                var (p, pp) = Fib(i - 1);
                return (p + pp, p);
            }
        }
        /// <summary>
        /// Simple use of local function
        /// </summary>
        /// <param name="number"></param>
        public void PrintNumbersUpto1(int number)
        {
            for (int i = number; i > 0; i--)
            {
                Console.WriteLine(PrintNumber(i));
            }

            string PrintNumber(int i) => $"The number is {i}.";
        }
    }
}
