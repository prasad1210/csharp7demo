﻿namespace CSharpNewFeatures.CSharp7
{
    public class OutputVariables
    {
        public void ReturnOutputVariables(string someInputString, out int someOutInteger)
        {
            someOutInteger = 10;
        }
    }
}
