﻿using static System.Console; // C#6.0 Feature - using static
using CSharpNewFeatures.CSharp7;

namespace CSharpNewFeatures
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var outputVariables = new OutputVariables();
            
            // Notice the inline declaration of integerOutput as var directly while calling
            outputVariables.ReturnOutputVariables("some test string", out var integerOutput);

            WriteLine($"Output value : {integerOutput}");

            var tuples = new Tuples();

            var unnamedTuple = tuples.GetPatientFromUnNamedTuples(1);
            // C#6.0 Feature : interpolated strings
            WriteLine($" From Un-Named Tuple: {unnamedTuple.Item1} {unnamedTuple.Item3}.");

            var namedTuple = tuples.GetPatientFromNamedTuples(1);
            WriteLine($" From Named Tuple: {namedTuple.first} {namedTuple.last}.");

            // Deconstucting declaration for tuples
            var (first, middle, last) = tuples.GetPatientFromNamedTuples(1);
            WriteLine($" Deconstructing declaration: {first} {last}.");

            var localFunctions = new LocalFunctions();

            WriteLine($" Fibonacci of 5: {localFunctions.Fibonacci(5)}");


            // Simple example of local functions
            localFunctions.PrintNumbersUpto1(10);

            

            ReadLine();
        }

        
    }
}


